const express = require('express');
const app = express();
var path    = require("path");

app.use('/css', express.static('public/css'));
app.use('/data', express.static('public/data'));
app.use('/js', express.static('public/js'));
app.use('/images', express.static('public/images'));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/pages/index.html'));
})

app.get('/graphMatrix', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/pages/graphMatrix.html'));
})

app.get('/graphChord', function (req, res) {
  res.sendFile(path.join(__dirname+'/public/pages/graphChord.html'));
})

app.get('/rapport', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/pages/rapport.html'));
})

app.listen(3000, function () {
  console.log('Project dataviz on port 3000');
})
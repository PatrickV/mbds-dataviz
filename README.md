# Installation
Commencez tout d'abord par cloner le projet à 
l'aide d'un terminal ou d'un IDE:
```
git clone https://bitbucket.org/PatrickV/mbds-dataviz.git
```

Les différentes étapes sont:
 - Installer nodeJS (https://nodejs.org/) avec NPM
 - Lancer les commandes suivantes dans un terminal en étant dans le dossier racine du projet:
```
npm install
npm start
```

C'est fini ! Le projet est maintenant disponible à l'adresse suivante : http://localhost:3000
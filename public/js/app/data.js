function fetchData() {

    d3.csv('data/police-cat-5000.csv', (csv) => {

        // Add Year attribute
        let normalized = [];
        for (let i = 0; i < csv.length; i++) {
            let row = csv[i];
            row.Year = moment(row.Date).year();
            normalized.push(row);
        }

        // Group incident by Year and District
        incidentGrouped = d3.nest()
            .key(function (d) { return d.Year; }).sortKeys(d3.ascending)
            .key((d) =>{
                return d.PdDistrict
            })
            .entries(normalized);

        for (let y = 0; y < incidentGrouped.length; y++) {
            var yearGroup = incidentGrouped[y];
            for (let m = 0; m < yearGroup.values.length; m++) {
                let solved = 0;
                let notSolved = 0;
                let district = yearGroup.values[m]

                for(var a = 0; a < district.values.length; a++){
                    let incident = district.values[a];

                    if(incident.Resolution !== 'NONE'){
                        solved++;
                    }else{
                        notSolved++;
                    }
                }
                district.resolu = solved;
                district.nonResolu = notSolved;
            }

        }

        run();
        refreshIntervalId = setInterval(run, delay);
    });
}

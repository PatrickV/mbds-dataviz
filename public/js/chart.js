document.addEventListener("DOMContentLoaded", function(event) {

    var w = 400,
        h = 400;
    
    var margin = {top: 50, right: 20, bottom: 70, left: 20};
    var pad = 80;
    var width = 2 * w + pad;

    var svg = d3.select('#chart')
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', h + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .attr('width', width)
        .attr('height', h);

    var corrplot = svg.append('g')
        .attr('id', 'corrplot');

    corrplot.append('text')
        .text('Correlation matrix')
        .attr('class', 'plottitle')
        .attr('x', w/2)
        .attr('y', -margin.top/2)
        .attr('dominant-baseline', 'middle')
        .attr('text-anchor', 'middle');

    var corXscale = d3.scaleBand().rangeRound([0,w]),
        corYscale = d3.scaleBand().rangeRound([h,0]),
        corColScale = d3.scaleLinear().domain([1, 0, -1]).range(['slateblue', 'white','crimson']);
    var corRscale = d3.scaleSqrt().domain([0,1]);

    d3.csv('data/corrMat.csv').then(function(data) {

        var nind = data.columns.length,
            nvar = data.length;

        corXscale.domain(d3.range(nind));
        corYscale.domain(d3.range(nind));
        corRscale.range([0,0.5*corXscale.bandwidth()]);

        var corr = [];
        for (var i = 0; i < data.length; ++i) {
            var keys = Object.keys(data[i]);
            for (var j = 0; j < keys.length; ++j) {
                corr.push({row: i, col: j, value: data[i][keys[j]]});
            }
        }

        var cells = corrplot.append('g')
            .attr('id', 'cells')
            .selectAll('empty')
            .data(corr)
            .enter().append('g')
            .attr('class', 'cell')
            .style('pointer-events', 'all');

        var rects = cells.append('rect')
            .attr('x', function(d) { return corXscale(d.col); })
            .attr('y', function(d) { return corXscale(d.row); })
            .attr('width', corXscale.bandwidth())
            .attr('height', corYscale.bandwidth())
            .attr('fill', 'none')
            .attr('stroke', 'none')
            .attr('stroke-width', '1');

        var circles = cells.append('circle')
            .attr('cx', function(d) {return corXscale(d.col) + 0.5*corXscale.bandwidth(); })
            .attr('cy', function(d) {return corXscale(d.row) + 0.5*corYscale.bandwidth(); })
            .attr('r', function(d) {return corRscale(Math.abs(d.value)); })
            .style('fill', function(d) { return corColScale(d.value); });

        corrplot.selectAll('g.cell').on('mouseover', function(d) {
            d3.select(this)
                .select('rect')
                .attr('stroke', 'black');

            var xPos = parseFloat(d3.select(this).select('rect').attr('x'));
            var yPos = parseFloat(d3.select(this).select('rect').attr('y'));
            
            var keys = Object.keys(data[0]);

            corrplot.append('text')
                .attr('class', 'corrlabel')
                .attr('x', corXscale(d.col))
                .attr('y', h + margin.bottom*0.2)
                .text(keys[d.col])
                .attr('dominant-baseline', 'middle')
                .attr('text-anchor', 'middle');

            corrplot.append('text')
                .attr('class', 'corrlabel')
                .text(keys[d.row])
                .attr('dominant-baseline', 'middle')
                .attr('text-anchor', 'middle')
                .attr('transform', 'translate(' + (-margin.left*0.1) + ',' + corXscale(d.row) + ')rotate(270)');

            corrplot.append('rect')
                .attr('class', 'tooltip')
                .attr('x', xPos + 10)
                .attr('y', yPos - 30)
                .attr('width', 40)
                .attr('height', 20)
                .attr('fill', 'rgba(200, 200, 200, 0.5)')
                .attr('stroke', 'black');

            corrplot.append('text')
                .attr('class', 'tooltip')
                .attr('x', xPos + 30)
                .attr('y', yPos - 15)
                .attr('text-anchor', 'middle')
                .attr('font-family', 'sans-serif')
                .attr('font-size', '14px')
                .attr('font-weight', 'bold')
                .attr('fill', 'black')
                .text(d3.format('.2f')(d.value));
        })
        .on('mouseout', function(d) {
            d3.select('#corrtext').remove();
            d3.selectAll('.corrlabel').remove();
            d3.select(this)
                .select('rect')
                .attr('stroke', 'none');
            //Hide the tooltip
            d3.selectAll('.tooltip').remove();
        });
    });
    
    var legend = svg.append("g");

    legend.attr("transform", "translate(" + (w + 50) + ")");

    var defs = legend.append('defs');

    var linearGradient = defs.append('linearGradient')
        .attr('id', 'linear-gradient');

    linearGradient
        .attr("x1", "0%")
        .attr("y1", "0%")
        .attr("x2", "0%")
        .attr("y2", "100%");

    linearGradient.selectAll("stop")
        .data([
            { offset: "0%", color: "slateblue" },
            { offset: "50%", color: "white" },
            { offset: "100%", color: "crimson" }
        ])
        .enter().append("stop")
        .attr("offset", function (d) {
            return d.offset;
        })
        .attr("stop-color", function (d) {
            return d.color;
        });

    legend.append("rect")
        .attr("width", 20)
        .attr("height", h)
        .style("fill", "url(#linear-gradient)");

    var xLeg = d3.scaleLinear()
        .domain([1, -1])
        .range([0, h]);

    var axisLeg = d3.axisRight(xLeg);

    legend.attr("class", "axis")
        .append("g")
        .attr("transform", "translate(15, 0)")
        .call(axisLeg);

});
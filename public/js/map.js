document.addEventListener("DOMContentLoaded", function(event) {

    /** IMPORTANT STUFF TO CHECK
    * http://bl.ocks.org/arpitnarechania/577bd1d188d66dd7dffb69340dc2d9c9
    * https://bl.ocks.org/kerryrodden/7090426
    **/

    // TODO : Corriger le nombre d'incident en cours et résolut (poursentage dans le camambert)

    var colors = {
        "UNSOLVED": '#BE4850',
        "SOLVED": '#6ab975'
    };


    var color = d3.scaleOrdinal([
        '#666D75',
        '#A4BB87',
        '#54C1B9',
        '#69799A',
        '#E77855',
        '#51434A',
        '#2E4C37',
        '#98A496',
        '#CFBE55',
        '#8A4E46',
        '#8BB9B5',
        '#A88851', 
        '#34484D',
        '#82CDCC',
        '#183D4F',
        '#DC4351',
        '#5A455E'
    ]);

    var width = 650;
    var height = 400;
    var radius = Math.min(width, height) / 2;


    var map = L.map('map').setView([37.761133, -122.441769], 13);
    L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> Contributors',
        maxZoom: 20,
    }).addTo(map);

    var svg = d3.select(map.getPanes().overlayPane).append("svg"),
    g = svg.append("g").attr("class", "leaflet-zoom-hide");

    d3.json("data/police-districts.geojson")
    .then(function(geoShape) {
        L.geoJSON(geoShape.features, {
            onEachFeature: onEachFeature
        }).addTo(map);

        var popup = L.popup();

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: function (e) {
                    mouseoverMap(e);
                },
                mouseout: function (e) {
                    mouseoutMap(e);
                },
                click: function (e) {
                    clickMap(e, feature);
                }
            });
        }

        function mouseoverMap (e) {

        }

        function mouseoutMap (e) {

        }

        function clickMap (e, feature) {
            var div = d3.create("div").attr('id', 'pie-popup');

            var title = div.append("h4")
            .attr('class', 'text-center');

            var colorCounter = 0;

            var district = feature.properties.district;

            d3.csv("data/police-cat-50000.csv")
                .then(function (data) {
                    var parsedData = [];
                    var key = [];
                    data.forEach(function (d) {
                        if (d.PdDistrict === district) {
                            var row = "";

                            if (d.Resolution === "NONE") {
                                row += "UNSOLVED-";
                            } else {
                                row += "SOLVED-";
                            }

                            var category = "";

                            if (d.Category.includes("-")) {
                                category += replaceAll("-", "_", d.Category);
                            } else {
                                category += d.Category;
                            }

                            if (colors[category] === undefined) {
                                if (colorCounter == color.range().length) {
                                    colorCounter = 0;
                                }

                                colors[category] = color(colorCounter);
                                colorCounter++;
                            }

                            row += category;

                            if (parsedData[row] === undefined) {
                                parsedData[row] = { row: row, count: 1 };
                                key.push(row);
                            } else {
                                parsedData[row].count += 1;
                            }
                        }
                    });

                    var totalCount = 0;
                    var arrangedData = [];
                    key.forEach(function (d) {
                        var elem = parsedData[d];
                        arrangedData.push([elem.row, elem.count]);
                        totalCount += elem.count;
                    });

                    title.text(feature.properties.district + " (" + totalCount + ")");

                    var json = buildHierarchy(arrangedData);
                    createVisualization(json);
                });

            function replaceAll(recherche, remplacement, chaineAModifier) {
                return chaineAModifier.split(recherche).join(remplacement);
            }

            // Total size of all segments; we set this later, after loading the data.
            var totalSize = 0;

            var svgPie = div.append("svg:svg")
                .attr('height', '400')
                .attr('width', '650');

            var vis = svgPie.append("svg:g")
                .attr("id", "container")
                .attr("transform", "translate(" + width / 3 + "," + height / 2 + ")");

            var partition = d3.partition()
                .size([2 * Math.PI, radius * radius]);

            var arc = d3.arc()
                .startAngle(function (d) { return d.x0; })
                .endAngle(function (d) { return d.x1; })
                .innerRadius(function (d) { return Math.sqrt(d.y0); })
                .outerRadius(function (d) { return Math.sqrt(d.y1); });


            // Main function to draw and set up the visualization, once we have the data.
            function createVisualization(json) {

                // Basic setup of page elements.
                drawLegend();

                // Bounding circle underneath the sunburst, to make it easier to detect
                // when the mouse leaves the parent g.
                vis.append("svg:circle")
                    .attr("r", radius)
                    .style("opacity", 0);

                // Turn the data into a d3 hierarchy and calculate the sums.
                var root = d3.hierarchy(json)
                    .sum(function (d) { return d.size; })
                    .sort(function (a, b) { return b.value - a.value; });

                // For efficiency, filter nodes to keep only those large enough to see.
                var nodes = partition(root).descendants()
                    .filter(function (d) {
                        return (d.x1 - d.x0 > 0.005); // 0.005 radians = 0.29 degrees
                    });

                var path = vis.data([json]).selectAll("path")
                    .data(nodes)
                    .enter().append("svg:path")
                    .attr("class", "pie-path")
                    .attr("display", function (d) { return d.depth ? null : "none"; })
                    .attr("d", arc)
                    .attr("fill-rule", "evenodd")
                    .style("fill", function (d) { return colors[d.data.name]; })
                    .style("opacity", 1)
                    .on("mouseover", mouseover);

                // Add the mouseleave handler to the bounding circle.
                d3.select("#container").on("mouseleave", mouseleave);

                // Get total size of the tree = value of root node from partition.
                totalSize = path.datum().value;
            };

            function getPecentageString(value, total) {
                var percentage = (100 * value / total).toPrecision(3);
                var percentageString = percentage + "%";
                if (percentage < 0.1) {
                    percentageString = "< 0.1%";
                }
                return percentageString;
            }

            // Fade all but the current sequence, and show it in the breadcrumb trail.
            function mouseover(d) {

                vis.select("#pie-label").remove();

                if (d.data.size !== undefined) {
                    var label = vis.append("text")
                        .attr('id', 'pie-label')
                        .attr("text-anchor", "middle")
                        .attr('font-size', '1.1em')
                    label.append("tspan")
                        .text(replaceAll("_", "-", d.data.name));
                    label.append("tspan")
                        .attr("text-anchor", "middle")
                        .attr("x", "0")
                        .attr('dy', '15')
                        .text(function () {
                            var cases = "cases";
                            if (d.data.size === 1) {
                                cases = "case";
                            }
                            return d.data.size + " " + cases + " or about " + getPecentageString(d.value, totalSize);
                        });
                } else {
                    var rowArray = d.data.children;
                    var totalElement = 0;

                    rowArray.forEach(function (line) {
                        totalElement += line.size;
                    });

                    var label = vis.append("text")
                        .attr('id', 'pie-label')
                        .attr("text-anchor", "middle")
                        .attr('font-size', '1.1em')
                    label.append("tspan")
                        .text(d.data.name);
                    label.append("tspan")
                        .attr("text-anchor", "middle")
                        .attr("x", "0")
                        .attr('dy', '15')
                        .text(getPecentageString(totalElement, totalSize));
                }

                var sequenceArray = d.ancestors().reverse();
                sequenceArray.shift(); // remove root node from the array

                // Fade all the segments.
                d3.selectAll(".pie-path")
                    .style("opacity", 0.3);

                // Then highlight only those that are an ancestor of the current segment.
                vis.selectAll("path")
                    .filter(function (node) {
                        return (sequenceArray.indexOf(node) >= 0);
                    })
                    .style("opacity", 1);
            }

            // Restore everything to full opacity when moving off the visualization.
            function mouseleave(d) {

                // Hide the breadcrumb trail
                d3.select("#trail")
                    .style("visibility", "hidden");

                // Deactivate all segments during transition.
                d3.selectAll(".pie-path").on("mouseover", null);

                // Transition each segment to full opacity and then reactivate it.
                d3.selectAll(".pie-path")
                    .transition()
                    .duration(1000)
                    .style("opacity", 1)
                    .on("end", function () {
                        d3.select(this).on("mouseover", mouseover);
                    });

                vis.select("#pie-label").remove();
            }

            function drawLegend() {

                var legendPie = svgPie.selectAll(".legend")
                    .data(d3.entries(colors))
                    .enter().append("g")
                    .attr("transform", function (d, i) {
                        return "translate(" + (width / 3 * 2) + "," + (i * 20 + 20) + ")";
                    })
                    .attr("class", "legend");

                legendPie.append("rect")
                    .attr("width", 15)
                    .attr("height", 15)
                    .attr("fill", function (d) {
                        return d.value;
                    });

                legendPie.append("text")
                    .text(function (d) {
                        return replaceAll("_", "-", d.key);
                    })
                    .attr("y", 10)
                    .attr("x", 20);
            }

            // Take a 2-column CSV and transform it into a hierarchical structure suitable
            // for a partition layout. The first column is a sequence of step names, from
            // root to leaf, separated by hyphens. The second column is a count of how 
            // often that sequence occurred.
            function buildHierarchy(csv) {
                var root = { "name": "root", "children": [] };
                for (var i = 0; i < csv.length; i++) {
                    var sequence = csv[i][0];
                    var size = +csv[i][1];
                    if (isNaN(size)) { // e.g. if this is a header row
                        continue;
                    }
                    var parts = sequence.split("-");
                    var currentNode = root;
                    for (var j = 0; j < parts.length; j++) {
                        var children = currentNode["children"];
                        var nodeName = parts[j];
                        var childNode;
                        if (j + 1 < parts.length) {
                            // Not yet at the end of the sequence; move down the tree.
                            var foundChild = false;
                            for (var k = 0; k < children.length; k++) {
                                if (children[k]["name"] == nodeName) {
                                    childNode = children[k];
                                    foundChild = true;
                                    break;
                                }
                            }
                            // If we don't already have a child node for this branch, create it.
                            if (!foundChild) {
                                childNode = { "name": nodeName, "children": [] };
                                children.push(childNode);
                            }
                            currentNode = childNode;
                        } else {
                            // Reached the end of the sequence; create a leaf node.
                            childNode = { "name": nodeName, "size": size };
                            children.push(childNode);
                        }
                    }
                }
                return root;
            };

            popup.setLatLng(e.latlng)
            .setContent(div.node())
            .openOn(map);
        }
    });
});
